

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    tablemodel.cpp

HEADERS += \
    mainwindow.h \
    tablemodel.h

QT       += network xml

CONFIG += openssl-linked
CONFIG += openssl
QT += core network sql svg webkit xml

TARGET = bbclient
TEMPLATE = app


