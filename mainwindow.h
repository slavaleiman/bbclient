#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "tablemodel.h"
#include <QtGui/QWidget>

#include <QLabel>
#include <QProgressBar>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDomElement>
#include <QPushButton>
#include <QListView>
#include <QDialog>
#include <QItemSelectionModel>

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:


private slots:
    void slotRequestFinished(QNetworkReply*);
    void type_login(void);
    void update_data();
    void changeCurrent(const QModelIndex &current);
    void open_url_slot(void);

private:
    //void get_user_repos_request(QString * username, QString * password);

    void send_request(QString * REQUEST_URL);

    void get_repos_events_request();
    void parseResourceEntry(const QDomElement &element);
    void parseEventsEntry(const QDomElement &element, int);

    QString USER;
    QString PASSWORD;

    bool m_req_finished;

    QDialog *loginDialog;
    QLineEdit *m_username;
    QLineEdit *m_password;
    QLabel *m_repo_info;
    QLabel *m_propertyLabel;
    QPushButton *m_login_btn;
    QPushButton *m_open_in_brwsr_btn;

    QListView *m_repos_view;

    QNetworkAccessManager *m_network;

    TableModel *table;
    QItemSelectionModel *selectionModel;

    QString currentRepoUrl;
};

#endif // MAINWINDOW_H


