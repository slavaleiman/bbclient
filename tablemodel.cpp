#include "tablemodel.h"
#include <QDebug>

TableModel::TableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

int TableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return RepoEntriesList.size();

}

int TableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 7;
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= RepoEntriesList.size() || index.row() < 0)
        return QVariant();

    if (role == Qt::DisplayRole) {
        QList<QString> repoEntry = RepoEntriesList.at(index.row());

        switch(index.column()){
        case 0: return repoEntry.at(0);
        case 1: return repoEntry.at(1);
        case 2: return repoEntry.at(2);
        case 3: return repoEntry.at(3);
        case 4: return repoEntry.at(4);
        case 5: return repoEntry.at(5);
        case 6: return repoEntry.at(6);
        case 7: return repoEntry.at(7);
        }
    }
    return QVariant();
}


bool TableModel::insertRows(int position, int rows, const QModelIndex &index)
{
    Q_UNUSED(index);
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row=0; row < rows; row++) {
        QList<QString> repoEntrylist;
        RepoEntriesList.insert(position,repoEntrylist);
    }

    endInsertRows();
    return true;
}


bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        int row = index.row();
        int col = index.column();
        QList<QString> p = RepoEntriesList.value(row);

        p.insert(col,value.toString());

        RepoEntriesList.replace(row, p);
        emit(dataChanged(index,index));

        return true;
    }
    return false;
}

