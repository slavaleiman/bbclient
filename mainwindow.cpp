
#include "mainwindow.h"
#include "tablemodel.h"

#include <QApplication>
#include <QDebug>
#include <QtCore/QUrl>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtXml/QDomDocument>
#include <QStringListModel>
#include <QListView>
#include <QDialog>
#include <QLineEdit>
#include <QMessageBox>
#include <QItemSelectionModel>
#include <QDesktopServices>
#include <QAbstractItemView>

const QString REQUEST_URL_PREFIX = "https://api.bitbucket.org/1.0/";
QString REPOS_REQUEST_URL = "user/repositories?format=xml";
//QString REPO_EVENT_REQUEST_URL = "repositories/slavaleiman/bb101repo/events/?format=xml";

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    //create model----------------------------------------------
    table = new TableModel(this);
    //----------------------------------------------------------
    setLayout(new QHBoxLayout());

    QVBoxLayout *repolistlayout = new QVBoxLayout(this);
    QVBoxLayout *middleLayout = new QVBoxLayout(this);
    QVBoxLayout *infolayout = new QVBoxLayout(this);

    m_repos_view = new QListView(this);
    m_repos_view->setFixedWidth(200);

    m_repo_info = new QLabel(this);

    m_repo_info->setFixedWidth(150);
    m_repo_info->setAlignment(Qt::AlignLeft | Qt::AlignTop);

    m_login_btn = new QPushButton("Login", this);
    m_open_in_brwsr_btn = new QPushButton("Open in browser", this);

    m_propertyLabel = new QLabel("Owner:\nSCM:\nIs_private:\nLast_update:\nLast_commit:",this);
    m_propertyLabel->setAlignment(Qt::AlignRight);

    repolistlayout->addWidget(m_repos_view);
    repolistlayout->addWidget(m_login_btn);

    middleLayout->addWidget(m_propertyLabel);

    infolayout->addWidget(m_repo_info);
    infolayout->addWidget(m_open_in_brwsr_btn);

    layout()->addItem(repolistlayout);
    layout()->addItem(middleLayout);
    layout()->addItem(infolayout);

    //---setmodel to view ------------------------------------------
    m_repos_view->setModel(table);
    connect(m_login_btn, SIGNAL(clicked()),this,SLOT(type_login()));

    selectionModel = m_repos_view->selectionModel();

    connect(m_open_in_brwsr_btn,SIGNAL(clicked()),this,SLOT(open_url_slot()));

}

MainWindow::~MainWindow()
{
}

void MainWindow::send_request(QString * REQUEST_URL)
{

    static int reqnum(0);

    m_network = new QNetworkAccessManager(this);
    QNetworkRequest request;
    request.setRawHeader("Authorization", "Basic " +
                         QByteArray(QString("%1:%2").arg(USER).arg(PASSWORD).toAscii()).toBase64());
    qDebug() << "sending request  " + QString(*REQUEST_URL);
    request.setUrl(QUrl(REQUEST_URL_PREFIX + *REQUEST_URL));

    QNetworkReply *reply = m_network->get(request);
    reply->setObjectName(QString::number(++reqnum));
//    while(!reply->isFinished())
//        QApplication::processEvents();

//    slotRequestFinished(reply);

    connect(m_network, SIGNAL(finished(QNetworkReply*)), this,
                     SLOT(slotRequestFinished(QNetworkReply*)));
//    connect(reply, SIGNAL(sslErrors(QList<QSslError>)),
//             this, SLOT(slotSslErrors(QList<QSslError>)));
}

void MainWindow::slotRequestFinished(QNetworkReply* reply)
{
    int row = reply->objectName().toInt();
    qDebug() << QString::number(row);
    if (reply->error() > 0) {
        qDebug() << "ERROR: " + reply->errorString();
        QMessageBox::warning(this,"ERROR: ", reply->errorString());
    }
    else {
        QByteArray data = reply->readAll();
        //qDebug() << data;

        QDomDocument doc;
        doc.setContent(data);
        //============================================================
        QDomElement root = doc.documentElement();
        if (root.tagName() != "response")
            return;
        QDomNode node = root.firstChild();

        while (!node.isNull()) {
            if (node.toElement().tagName() == "resource"){
                //qDebug() << "add new item to repo list";
                parseResourceEntry(node.toElement());
            }
            node = node.nextSibling();
        }
        node = root.firstChild();
        if (node.toElement().tagName() == "count"){
            node = node.nextSibling();
            if(node.toElement().tagName() == "events"){
                //qDebug() << "from events";
                node = node.toElement().firstChild();
                if(node.toElement().tagName() == "resource"){
                    //qDebug() << "events parsing";
                    parseEventsEntry(node.toElement(),row);
                }
            }
        }
    }
    m_req_finished = true;
}

void MainWindow::parseResourceEntry(const QDomElement &element)
{
    m_repos_view->setDisabled(true);
    disconnect(selectionModel, SIGNAL(currentChanged(QModelIndex,QModelIndex)),
               this,SLOT(changeCurrent(QModelIndex)));
    //QString secColumnData;
    QDomNode node = element.firstChild();
    int row = table->rowCount(QModelIndex());
    while (!node.isNull()) {
        if (node.toElement().tagName() == "owner") {

            table->insertRow(row);
            table->setData(table->index(row, 1), node.firstChild().toText().data() ,Qt::EditRole);
        }
        node = node.nextSibling();
        if (node.toElement().tagName() == "scm") {
            table->setData(table->index(row, 2), node.firstChild().toText().data() ,Qt::EditRole);
        }
        node = node.nextSibling();

        if (node.toElement().tagName() == "slug") {

            table->setData(table->index(row, 0) , node.firstChild().toText().data(),Qt::EditRole);

            QModelIndex repo_index = table->index(row, 0);

            QString requrl = "repositories/" + USER + "/" +
                    table->data(repo_index,Qt::DisplayRole).toString() + "/events/?format=xml";
            send_request(&requrl);
        }
        node = node.nextSibling();
        if (node.toElement().tagName() == "is_private") {
            table->setData(table->index(row, 3), node.firstChild().toText().data()
                           ,Qt::EditRole);
        }
    }
}

void MainWindow::parseEventsEntry(const QDomElement &element,int row)
{
    //static int row(0);
    //row++;
    qDebug()<< row;
    QDomNode node = element.firstChild();

    while (node.toElement().tagName() != "event") {
        node = node.nextSibling();
    }
    if(node.firstChild().toText().data() == "commit"){
        while(!node.isNull()){
            if (node.toElement().tagName() == "description") {

                if(table->setData(table->index(row-2, 4), node.firstChild().toText().data(), Qt::EditRole))

                qDebug() << node.firstChild().toText().data();
            }
            node = node.previousSibling();
        }

        node = element.firstChild();


        while(!node.isNull()){
            if (node.toElement().tagName() == "repository") {
                node = node.toElement().firstChild();
                while(1){
                    if(node.toElement().tagName() == "last_updated") {

                        table->setData(table->index(row-2, 5), node.firstChild().toText().data(), Qt::EditRole);

                        qDebug() << "last_update: " << node.firstChild().toText().data();
                        break;
                    }
                    node = node.nextSibling();
                }
            }
            node = node.nextSibling();
        }

    }
    m_repos_view->setEnabled(true);
connect(selectionModel, SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(changeCurrent(QModelIndex)));
}


void MainWindow::type_login(void)
{
    loginDialog = new QDialog(this);
    loginDialog->setWindowTitle(tr("Enter your username and password"));

    loginDialog->setLayout(new QVBoxLayout(loginDialog));

    QHBoxLayout *input1 = new QHBoxLayout();
    QHBoxLayout *input2 = new QHBoxLayout();

    m_username = new QLineEdit("slavaleiman", loginDialog);
    //m_username->text() = "your username";
    m_password = new QLineEdit("passport", loginDialog);
    m_password->setEchoMode(QLineEdit::Password);
    m_password->text() = "your password";
    QPushButton *btnLogin = new QPushButton("Login", loginDialog);

    input1->addWidget(new QLabel("User name ", loginDialog));
    input1->addWidget(m_username);
    input2->addWidget(new QLabel("Password  ", loginDialog));
    input2->addWidget(m_password);

    loginDialog->layout()->addItem(input1);
    loginDialog->layout()->addItem(input2);
    loginDialog->layout()->addWidget(btnLogin);

    connect(btnLogin,SIGNAL(clicked()),loginDialog,SLOT(close()));
    connect(btnLogin,SIGNAL(clicked()),this,SLOT(update_data()));

    loginDialog->exec();
}

void MainWindow::update_data(void)
{
    USER = m_username->text();
    PASSWORD = m_password->text();

    //qDebug() << "login = " << m_username->text();
    //qDebug() << "password = " << m_password->text();

    send_request(&REPOS_REQUEST_URL);
}


void MainWindow::changeCurrent(const QModelIndex &current)
{
    if(current.isValid() && current.row() <= table->rowCount(QModelIndex())){
        m_repo_info->setText(table->index(current.row(),1).data().toString() + "\n" +
                             table->index(current.row(),2).data().toString() + "\n" +
                             table->index(current.row(),3).data().toString() + "\n" +
                             table->index(current.row(),5).data().toString() + "\n" +
                             table->index(current.row(),4).data().toString());

        currentRepoUrl = "http://bitbucket.org/" + table->index(current.row(),1).data().toString() + "/" +
                table->index(current.row(),0).data().toString();
    }
}

void MainWindow::open_url_slot(void)
{
    qDebug() << "opening url  " + currentRepoUrl;
    QDesktopServices::openUrl(QUrl(currentRepoUrl));

}
